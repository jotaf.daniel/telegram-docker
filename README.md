# Dockerized Telegram

This is a dockerized version of Telegram Desktop client


## Instructions

The script shall be enough to run the client in your machine.
Please, ensure:
- you have added you user to the group `docker` so that you can run docker commands simply by `docker <command>` instead of using `sudo docker <command>`
- the script has permission to execute - if not, run `(sudo) chmod 755 telegram.sh`

When it's all OK, run:

```
./telegram.sh
```


You may also want to let your script to be globally accessible. For that, you may place a copy of the script within `$PATH`


For instance:

```
(sudo) cp telegram.sh /usr/bin/telegram
```

This will enable you to run directly with `telegram` from any directory - this will be globally executable.


## Contributing

If you want to tweak the image, the Dockerfile is available [here](https://gitlab.com/jotaf.daniel/telegram-docker/blob/master/Dockerfile)
