#!/bin/bash

xhost +local:root

LINES=`docker container ls -af name=telegram | grep telegram | wc -l`

if [[ ! $LINES = "1" ]]; then
  docker container run -it \
    --env="DISPLAY" \
    -e QT_X11_NO_MITSHM=1 \
    -e TZ=America/Sao_Paulo \
    -v /tmp/.X11-unix:/tmp/.X11-unix:rw \
    -v $HOME:/home/telegram \
    --name telegram -d jooaodanieel/telegram
else
  docker container start telegram
fi
