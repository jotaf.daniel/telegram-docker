FROM base/archlinux

RUN pacman -Syyu --noconfirm \
    && pacman -S telegram-desktop --noconfirm

WORKDIR /home/telegram

CMD telegram-desktop
